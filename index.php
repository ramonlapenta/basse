<!DOCTYPE html>
<html lang="es">
<head>
<base href="http://basse.local:8080/" />
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>Basse</title>
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
<!--[if lt IE 9]>
    <script src="js/html5.js"></script>
    <link rel="stylesheet" href="css/ie.css" />
<![endif]-->
<!-- <div id="sd_alert" class="sd_alert_warning">Esto es un mensaje</div> -->
    <div id="wrap">
        <div id="head">
            <h1><a href="home">Basse Generator</a></h1>
            <ul class="main_menu">
                <li class="active"><a href="#">Opción 1</a></li>
                <li><a href="#">Opción 2</a></li>
                <li><a href="#">Opción 3</a></li>
                <li><a href="#">Opción 4</a></li>
                <li><a href="#">Opción 5</a></li>
            </ul>
        </div>
        <div id="main">
            <h1>Definición de Estilos</h1>
            <h2 class="basse-h2">SlideShow</h2>
            <div>
                <p>Con ID <strong>"slider"</strong> para el contenedor (para estilizar si hace falta) y <strong>"slides"</strong> para el cycle. Si no se desea colocar el pager hay que quitar la opcion del script y el div #slidepager.</p>
                <div class="carousel">
                    <div class="slides">
                        <div>
                            <a href=""><img src="images/slider/image1.jpg" alt="Imagen 1" width="942" height="240" /></a>
                            <p>Mensaje del slide 1</p>
                        </div>
                        <div>
                            <a href=""><img src="images/slider/image2.jpg" alt="Imagen 2" width="942" height="240" /></a>
                            <p>Mensaje del slide 2</p>
                        </div>
                    </div>
                    <div id="slidepager"></div>
                </div>
            </div>
            <div class="clear"></div>
            <h2 class="basse-h2">Estilos de Texto</h2>
            <div class="col6">
                <h2>Título H2</h2>
                <h3>Título H3</h3>
                <p>Quisque a venenatis augue. Nunc pretium lorem vel lectus congue vel eleifend enim auctor. Sed consequat bibendum eros et tincidunt.</p>
                <h4>Título H4</h4>
                <p>Curabitur a dui id metus pulvinar commodo vitae tempor odio. Vestibulum vitae viverra eros. Mauris purus purus, tristique vel semper eu, molestie non velit.</p>
                <h5>Título H5</h5>
                <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                <h6>Título H6</h6>
                <p>Aenean lobortis, lectus at facilisis fermentum, ante enim malesuada erat, pretium porttitor magna ipsum ac libero. </p>
                <p class="txtr"><strong>(.txtr)</strong> Aenean lobortis, lectus at facilisis fermentum, ante enim malesuada erat, pretium porttitor magna ipsum ac libero. </p>
                <p class="txtc"><strong>(.txtc)</strong> Aenean lobortis, lectus at facilisis fermentum, ante enim malesuada erat, pretium porttitor magna ipsum ac libero. </p>
                <h4>Errores en Texto</h4>
                <p class="sd_p_msg p_error">Esto es un error<p>
                <p class="sd_p_msg p_warning">Esto es una advertencia<p>
                <p class="sd_p_msg p_help">Esto es una ayuda<p>
                <p class="sd_p_msg p_success">Esto es un mensaje de exito<p>
            </div>
            <div class="col6 colr">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <a href="#">esto es un link</a>, Maecenas in odio arcu. <strong>esto es strong</strong>, Morbi tempus volutpat <em>esto es em</em> consectetur. Proin ut diam in erat cursus aliquet ut ac neque.</p>
                <h3>Listas</h3>
                <div class="col3">
                    <h4>Lista Desordenada</h4>
                    <ul>
                        <li>Esto es una lista</li>
                        <li>Esto es una lista
                            <ul>
                                <li>Una lista anidada</li>
                                <li>Una lista anidada</li>
                            </ul>
                        </li>
                        <li>Esto es una lista</li>
                    </ul>                    
                    <h4>Lista Ordenada</h4>
                    <ol>
                        <li>Esto es una lista</li>
                        <li>Esto es una lista
                            <ol>
                                <li>Una lista anidada</li>
                                <li>Una lista anidada</li>
                            </ol>
                        </li>
                        <li>Esto es una lista</li>
                    </ol>                    
                </div>
                <div class="col3 colr">
                    <h4>Lista de Menu (.options)</h4>
                    <ul class="options">
                        <li class="current"><a href="#">Item de Options</a></li>
                        <li><a href="#">Item de Options</a></li>
                        <li><a href="#">Item de Options</a></li>
                        <li><a href="#">Item de Options</a></li>
                        <li><a href="#">Item de Options</a></li>
                    </ul>
                </div>
                <div class="clear"></div>
                <h3>Pre</h3>
                <pre>localStorage.setItem('name', 'value');</pre>
                <h3>Blockquote</h3>
                <blockquote>
                    <p>Esto es un blockquote Etiam consequat auctor hendrerit. Nulla orci diam, porta eu condimentum et, tincidunt vitae augue.</p>
                </blockquote>
            </div>
            <div class="clear"></div>
            <h2 class="basse-h2">Estilos de Imagenes</h2>
            <div class="col6">
                <p><img src="images/nature1.jpg" alt="Image 1" width="240" height="160" class="imgl imgb" /> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque nulla velit, pretium vitae aliquam sit amet, elementum nec augue. Curabitur sodales nunc at neque mattis blandit. Mauris nec viverra arcu. Donec ac diam magna. Sed sed massa ut leo congue dapibus. Praesent fringilla lobortis mauris id condimentum. Sed et ullamcorper lectus. In suscipit orci in nibh semper ut imperdiet tellus sodales. Fusce mattis enim ut mauris luctus ut pharetra sapien aliquet. Quisque ullamcorper, leo ac posuere pulvinar, risus turpis viverra enim, non pellentesque nulla lectus ac diam. Curabitur mollis sagittis est, ut dapibus nisi egestas non. Etiam purus nunc, molestie id laoreet nec, posuere non mauris. Mauris fermentum ligula vitae velit venenatis id pharetra nisi ultricies. Aenean neque magna, semper a placerat vitae, dignissim quis augue. Quisque quis ligula vel nibh imperdiet fermentum. Suspendisse potenti. </p>
                <p><img src="images/nature3.jpg" alt="Image 3" width="240" height="160" class="imgc imgb" /> Praesent interdum faucibus felis eget dictum. Maecenas volutpat sapien in ante dignissim aliquam. Ut adipiscing dapibus leo eget pharetra. Etiam ac nisi justo, a ultrices lorem. Maecenas euismod volutpat ligula, quis auctor ante vehicula at. Cras adipiscing arcu eget enim volutpat dictum. Suspendisse at neque id massa tempus porta fermentum vel eros. Aenean arcu diam, dictum quis hendrerit ac, interdum egestas purus. Pellentesque lacinia blandit ultrices. Proin malesuada dignissim lorem ut dapibus. </p>
            </div>
            <div class="col6 colr">
                <p><img src="images/nature2.jpg" alt="Image 2" width="240" height="160" class="imgr imgb" /> Etiam dui nisl, dignissim pretium consectetur sollicitudin, pretium luctus turpis. Ut luctus, lorem id lacinia ullamcorper, arcu velit feugiat massa, a luctus elit nulla vel metus. Aliquam erat volutpat. Maecenas ut mauris at metus pretium tempus at vel nisi. Nunc nisi diam, varius ac pellentesque vel, feugiat vitae orci. Nulla at ligula ligula. Praesent eget posuere purus. Cras eu est sed mi pulvinar luctus et consequat massa. Praesent ornare dictum tortor a pharetra. Aliquam scelerisque sapien quis risus ultricies lacinia. Cras posuere egestas consectetur. Aliquam erat volutpat. Sed mollis, nisl volutpat dignissim mollis, dolor magna varius nisi, nec placerat lectus urna eu sapien. Suspendisse vel est quis eros elementum mollis a at lorem. Suspendisse potenti. Suspendisse potenti. </p>
                <p>Cras ac felis et leo suscipit lacinia. Curabitur aliquet scelerisque justo, ac pharetra tortor feugiat a. Nulla auctor eros at felis blandit non vehicula nibh gravida. Maecenas eget semper eros. Nam ac libero elit. Maecenas interdum tristique neque. Vivamus nulla justo, lacinia eget semper nec, ultricies sed ante. Aliquam pellentesque congue mi, nec tincidunt urna aliquam ut. Sed a magna magna. Nunc non faucibus orci. Nulla at justo libero, ut scelerisque enim. Duis vel dolor ut odio aliquet blandit in et purus. Curabitur a porttitor turpis. </p>
                <p class="pager">
                    <a class="current" href="#">1</a>
                    <a href="#">2</a>
                    <a href="#">3</a>
                    <a href="#">4</a>
                </p>
            </div>
            <div class="clear"></div>
            <h2 class="basse-h2">Column Test</h2>
            <div class="coltest">
                <div class="col2"><p>Col 2</p></div>
                <div class="col2"><p>Col 2</p></div>
                <div class="col2"><p>Col 2</p></div>
                <div class="col2"><p>Col 2</p></div>
                <div class="col2"><p>Col 2</p></div>
                <div class="col2 colr"><p>Col 2</p></div>
            </div>
            <div class="clear"></div>
            <div class="coltest">
                <div class="col3"><p>Col 3</p></div>
                <div class="col3"><p>Col 3</p></div>
                <div class="col3"><p>Col 3</p></div>
                <div class="col3 colr"><p>Col 3</p></div>
            </div>
            <div class="clear"></div>
            <div class="coltest">
                <div class="col4"><p>Col 4</p></div>
                <div class="col4"><p>Col 4</p></div>
                <div class="col4 colr"><p>Col 4</p></div>
            </div>
            <div class="clear"></div>
            <div class="coltest">
                <div class="col6"><p>Col 6</p></div>
                <div class="col6 colr"><p>Col 6</p></div>
            </div>
            <div class="clear"></div>
            <div class="coltest">
                <div class="col5"><p>Col 5</p></div>
                <div class="col7 colr"><p>Col 7</p></div>
            </div>
            <div class="clear"></div>
            <div class="coltest">
                <div class="col4"><p>Col 4</p></div>
                <div class="col8 colr"><p>Col 8</p></div>
            </div>
            <div class="clear"></div>
            <div class="coltest">
                <div class="col3"><p>Col 3</p></div>
                <div class="col9 colr"><p>Col 9</p></div>
            </div>
            <div class="clear"></div>
            <div class="coltest">
                <div class="col2"><p>Col 2</p></div>
                <div class="col10 colr"><p>Col 10</p></div>
            </div>
            <div class="clear"></div>
            <div class="coltest">
                <div class="col3"><p>Col 3</p></div>
                <div class="col3"><p>Col 3</p></div>
                <div class="col6 colr"><p>Col 6</p></div>
            </div>
            <div class="clear"></div>
            <div class="coltest">
                <div class="col3"><p>Col 3</p></div>
                <div class="col6"><p>Col 6</p></div>
                <div class="col3 colr"><p>Col 3</p></div>
            </div>
            <div class="clear"></div>
            <h2 class="basse-h2">Tablas</h2>
            <div class="col8">
                <h3>Tabla Simple</h3>
                <table width="100%">
                    <thead>
                        <tr>
                            <th>Columna 1</th>
                            <th>Columna 2</th>
                            <th>Columna 3</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Celda 1</td>
                            <td>Celda 2</td>
                            <td>Celda 3</td>
                        </tr>
                        <tr>
                            <td>Celda 1</td>
                            <td>Celda 2</td>
                            <td>Celda 3</td>
                        </tr>
                        <tr>
                            <td>Celda 1</td>
                            <td>Celda 2</td>
                            <td>Celda 3</td>
                        </tr>
                    </tbody>
                </table>
                <h3>Tabla con estilos (.table-list)</h3>
                <table width="100%" class="table-list">
                    <thead>
                        <tr>
                            <th>Columna 1</th>
                            <th class="txt_center">Columna 2 (.txt_center)</th>
                            <th class="txt_right">Columna 3 (.txt_right)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="tr-odd">
                            <td>Celda 1</td>
                            <td class="txt_center">Celda 2</td>
                            <td class="txt_right">Celda 3</td>
                        </tr>
                        <tr class="tr-even">
                            <td>Celda 1</td>
                            <td class="txt_center">Celda 2</td>
                            <td class="txt_right">Celda 3</td>
                        </tr>
                        <tr class="tr-odd">
                            <td>Celda 1</td>
                            <td class="txt_center">Celda 2</td>
                            <td class="txt_right">Celda 3</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col4 colr">
                <h3>Tablas</h3>
                <p>A la tabla simple unicamente se le han eliminado los boders, padding y cellspacing.</p>
                <p>La tabla ".table-list" automaticamente agrega estilo zebra, tanto con CSS3 (nth-of-type(odd)) como con la clase .tr-odd y .tr-even.</p>
            </div>
            <div class="clear"></div>
            <h2 class="basse-h2">Formularios</h2>
            <div class="col4">
                <h3>Ejemplo Form Pequeño</h3>
                <form id="contacto" method="post" action="" class="form form_small">
                    <input type="hidden" name="contactos[csrf_token]" id="contactos_csrf_token" value="r0rzvruupyslb0j41rbsbtbl" />
                    <fieldset>
                        <legend>Formulario de Contacto</legend>
                        <div id="field_contactos_nombre" class="sd_field sd_string sd_required sd_error">
                            <label for="contactos_nombre">Nombre:</label>
                            <input type="text" name="contactos[nombre]" id="contactos_nombre" value="" />
                            <p class="sd_error">Es obligatorio</p>
                        </div>
                        <div id="field_contactos_email" class="sd_field sd_email sd_required">
                            <label for="contactos_email">Email:</label>
                            <input type="email" name="contactos[email]" id="contactos_email" value="" />
                        </div>
                        <div id="field_contactos_telefono" class="sd_field sd_string">
                            <label for="contactos_telefono">Tel&eacute;fono de la última empresa donde ha trabajado:</label>
                            <p class="sd_help">Colocar telefono valido y actual</p>
                            <input type="text" name="contactos[telefono]" id="contactos_telefono" value="" />
                        </div>
                        <div id="field_contactos_empresa" class="sd_field sd_autocomplete sd_string">
                            <label for="contactos_empresa">Empresa:</label>
                            <input type="text" name="contactos[empresa]" id="contactos_empresa" value="" />
                        </div>
                        <div id="field_contactos_web" class="sd_field sd_url sd_string">
                            <label for="contactos_web">Empresa:</label>
                            <input type="url" name="contactos[web]" id="contactos_web" value="" />
                        </div>
                        <div id="field_contactos_mensaje" class="sd_field sd_textarea sd_required">
                            <label for="contactos_mensaje">Mensaje:</label>
                            <textarea name="contactos[mensaje]" id="contactos_mensaje"></textarea>
                        </div>
                        <div class="sd_field sd_checkbox sd_required">
                            <p>Checkboxes</p>
                            <label><input type="checkbox" name="checkbox[radio2]" id="contactos_checkbox2_1" value="" /> Checkbox Uno </label>
                            <label><input type="checkbox" name="checkbox[radio2]" id="contactos_checkbox2_2" value="" /> Checkbox Dos </label>
                            <label><input type="checkbox" name="checkbox[radio2]" id="contactos_checkbox2_3" value="" /> Checkbox Tres con mucho mucho mas texto en la etiqueta para ver como sale.</label>
                        </div>
                    </fieldset>
                    <fieldset>
                        <p>Gracias por comunicarse con nosotros. <button type="submit">Enviar Mensaje</button></p>
                    </fieldset>
                </form>
            </div>
            <div class="col6">
                <h3>Ejemplo Form Mediano</h3>
                <form id="contacto2" method="post" action="" class="form form_medium">
                    <input type="hidden" name="contactos[csrf_token2]" id="contactos_csrf_token2" value="r0rzvruupyslb0j41rbsbtbl" />
                    <fieldset>
                        <legend>Formulario de Contacto</legend>
                        <div id="field_contactos_nombre2" class="sd_field sd_string sd_required sd_error">
                            <label for="contactos_nombre2">Nombre:</label>
                            <input type="text" name="contactos[nombre2]" id="contactos_nombre2" value="" />
                            <p class="sd_error">Es obligatorio</p>
                        </div>
                        <div id="field_contactos_email2" class="sd_field sd_email sd_required">
                            <label for="contactos_email2">Email:</label>
                            <input type="email" name="contactos[email2]" id="contactos_email2" value="" />
                        </div>
                        <div id="field_contactos_telefono2" class="sd_field sd_string">
                            <p class="sd_help">Colocar telefono valido y actual en el que uno pueda llamarlo y atienda.</p>
                            <label for="contactos_telefono2">Tel&eacute;fono:</label>
                            <input type="text" name="contactos[telefono2]" id="contactos_telefono2" value="" />
                        </div>
                        <div id="field_contactos_empresa2" class="sd_field sd_string sd_long_label">
                            <label for="contactos_empresa2">Empresa en la que trabajó haciendo pasantías:</label>
                            <input type="text" name="contactos[empresa2]" id="contactos_empresa2" value="" />
                        </div>
                        <div id="field_contactos_mensaje2" class="sd_field sd_textarea sd_required">
                            <label for="contactos_mensaje2">Mensaje:</label>
                            <textarea name="contactos[mensaje2]" id="contactos_mensaje2"></textarea>
                        </div>
                        <div id="field_contactos_fecha" class="sd_field sd_date">
                            <label for="contactos_fecha">Fecha:</label>
                            <input type="date" name="contactos[fecha]" id="contactos_fecha" />
                        </div>
                        <div class="sd_field sd_radios sd_required">
                            <p>Radios</p>
                            <label><input type="radio" name="contactos[radio2]" id="contactos_radio2_1" value="" /> Radio Uno </label>
                            <label><input type="radio" name="contactos[radio2]" id="contactos_radio2_2" value="" /> Radio Dos </label>
                            <label><input type="radio" name="contactos[radio2]" id="contactos_radio2_3" value="" /> Radio Tres con mucho mucho mas texto en la etiqueta para ver como sale.</label>
                        </div>
                    </fieldset>
                    <fieldset>
                        <p>Usando input submit. <input type="submit" name="msg" value="Enviar Mensaje" /></p>
                    </fieldset>
                </form>
            </div>
            <div class="col2 colr">
                <h3>Ayuda</h3>
                <p>Las clases usadas son "<strong>form-small</strong>" y<br /> "<strong>form-medium</strong>". Las medidas de cada elemento estan definidas en el grid.</p>
            </div>
            <div class="clear"></div>
            <div class="col8">
                <h3>Ejemplo Form Grande</h3>
                <form id="contacto3" method="post" action="" class="form form_large">
                    <input type="hidden" name="contactos[csrf_token]" id="contactos_csrf_token3" value="r0rzvruupyslb0j41rbsbtbl" />
                    <fieldset>
                        <legend>Formulario de Contacto</legend>
                        <div id="field_contactos_nombre3" class="sd_field sd_string sd_required">
                            <label for="contactos_nombre">Nombre:</label>
                            <input type="text" name="contactos[nombre]" id="contactos_nombre3" value="" />
                        </div>
                        <div id="field_contactos_email3" class="sd_field sd_email sd_required">
                            <label for="contactos_email">Email:</label>
                            <input type="email" name="contactos[email]" id="contactos_email3" value="" />
                        </div>
                    </fieldset>
                    <fieldset>
                        <div id="field_contactos_telefono3" class="sd_field sd_string">
                            <label for="contactos_telefono">Tel&eacute;fono:</label>
                            <input type="text" name="contactos[telefono]" id="contactos_telefono3" value="" />
                        </div>
                        <div id="field_contactos_empresa3" class="sd_field sd_string">
                            <label for="contactos_empresa">Empresa:</label>
                            <input type="text" name="contactos[empresa]" id="contactos_empresa3" value="" />
                        </div>
                        <div id="field_contactos_fecha2" class="sd_field sd_date">
                            <label for="contactos_fecha2">Fecha cuando se dio cuenta de que se iba a dedicar a esto por el resto de su vida:</label>
                            <p class="sd_help">Formato: DD-MM-AAAA</p>
                            <input type="date" name="contactos[fecha2]" id="contactos_fecha2" />
                        </div>
                    </fieldset>
                    <div class="clear"></div>
                    <fieldset class="wide">
                        <div id="field_campo3" class="sd_field sd_string">
                            <label for="contactos_campo3">Campo 3:</label>
                            <input type="text" name="contactos[campo3]" id="contactos_campo3" value="" />
                        </div>
                        <div id="field_campo4" class="sd_field sd_string sd_long_label">
                            <label for="contactos_campo4">Campo 3 con etiqueta larga tambien para ver que salga bien:</label>
                            <input type="text" name="contactos[campo4]" id="contactos_campo4" value="" />
                        </div>
                        <div id="field_contactos_mensaje3" class="sd_field sd_textarea sd_required">
                            <label for="contactos_mensaje">Mensaje:</label>
                            <textarea name="contactos[mensaje]" id="contactos_mensaje3"></textarea>
                        </div>
                    </fieldset>
                    <fieldset class="wide">
                        <p>Gracias por comunicarse con nosotros. <button type="submit">Enviar Mensaje</button></p>
                    </fieldset>
                </form>
            </div>
            <div class="col4 colr">
                <h3>Ayuda</h3>
                <p>La clase usada es "<strong>form-large</strong>". Las medidas de cada elemento estan definidas en el grid y el archivo del formulario.</p>
            </div>
            <div class="clear"></div>
            <div>
                <h3>Ejemplo Form Full Size</h3>
                <p>La clase usada es "<strong>form-full</strong>". Las medidas de cada elemento estan definidas en el grid y el archivo del formulario.</p>
                <form id="contacto4" method="post" action="" class="form form_full">
                    <input type="hidden" name="contactos[csrf_token]" id="contactos_csrf_token4" value="r0rzvruupyslb0j41rbsbtbl" />
                    <fieldset>
                        <legend>Formulario de Contacto</legend>
                        <div id="field_contactos_nombre4" class="sd_field sd_string sd_required">
                            <label for="contactos_nombre">Nombre:</label>
                            <input type="text" name="contactos[nombre]" id="contactos_nombre4" value="" />
                        </div>
                        <div id="field_contactos_email4" class="sd_field sd_email sd_required">
                            <label for="contactos_email">Email:</label>
                            <input type="email" name="contactos[email]" id="contactos_email4" value="" />
                        </div>
                        <div id="field_campo5" class="sd_field sd_string sd_long_label">
                            <label for="contactos_campo5">Campo 5 con etiqueta larga para que salga bien:</label>
                            <input type="text" name="contactos[campo5]" id="contactos_campo5" value="" />
                        </div>
                    </fieldset>
                    <fieldset>
                        <div id="field_contactos_telefono4" class="sd_field sd_string">
                            <label for="contactos_telefono">Tel&eacute;fono:</label>
                            <input type="text" name="contactos[telefono]" id="contactos_telefono4" value="" />
                        </div>
                        <div id="field_contactos_empresa4" class="sd_field sd_string">
                            <label for="contactos_empresa">Empresa:</label>
                            <input type="text" name="contactos[empresa]" id="contactos_empresa4" value="" />
                        </div>
                    </fieldset>
                    <div class="clear"></div>
                    <fieldset class="wide">
                        <div id="field_campo6" class="sd_field sd_string">
                            <label for="contactos_campo6">Campo 6:</label>
                            <input type="text" name="contactos[campo6]" id="contactos_campo6" value="" />
                        </div>
                        <div id="field_campo7" class="sd_field sd_string sd_long_label">
                            <label for="contactos_campo7">Campo 7 con etiqueta larga para que salga bien:</label>
                            <input type="text" name="contactos[campo7]" id="contactos_campo7" value="" />
                        </div>
                        <div id="field_contactos_mensaje4" class="sd_field sd_textarea sd_required">
                            <label for="contactos_mensaje">Mensaje:</label>
                            <textarea name="contactos[mensaje]" id="contactos_mensaje4"></textarea>
                        </div>
                        <div id="field_contactos_mensaje5" class="sd_field sd_textarea sd_long_label">
                            <label for="contactos_mensaje5">Observaciones encontradas en el elemento:</label>
                            <textarea name="contactos[mensaje5]" id="contactos_mensaje5"></textarea>
                        </div>
                    </fieldset>
                    <fieldset class="wide">
                        <p>Gracias por comunicarse con nosotros. <button type="submit">Enviar Mensaje</button></p>
                    </fieldset>
                </form>
            </div>
            <div class="clear"></div>
        </div>
        <div id="foot">
            <p class="fl">Basse Framework Copyright &copy; 2011 <a href="http://www.icad.com.ve" class="fl-icad" target="_blank" title="Desarrollado por iCad">icad</a> <a href="/webmail" class="fl-mail" title="Webmail">webmail</a> <a href="/admin" class="fl-admi" title="Admin">Adm</a></p>
        </div>
    </div>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('js/jquery.1.7.1.min.js"><\/script>')</script>
  <script src="js/easing.js"></script>
  <script src="js/cycle.js"></script>
  <script src="js/scripts.js"></script>
</body>
</html>
